const expect = require("chai").expect;
const rewire = require("rewire");
const order = rewire("../lib/order");

describe("Ordering Items", function() {
  beforeEach(function() {
    this.testData = [
      {sku: "AAA", qty: "10"},
      {sku: "BBB", qty: "3"},
      {sku: "CCC", qty: "5"}
    ];
    order.__set__ ("inventoryData", this.testData);
  });
  it("order an item when they are enough in stock", function() {
    order.orderItem("CCC", 5, function (done) {
      done();
    });
  });
})