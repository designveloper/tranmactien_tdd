module.exports = {
  printName(person) {
    return `${person.lastName}, ${person.firstName}`;
  }
}